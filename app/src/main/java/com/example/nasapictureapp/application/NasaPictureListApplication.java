package com.example.nasapictureapp.application;

import com.example.nasapictureapp.dependencyinjection.AppComponent;
import com.example.nasapictureapp.dependencyinjection.AppInjector;

import android.app.Activity;
import android.app.Application;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

public class NasaPictureListApplication extends Application implements HasActivityInjector {

    private static AppComponent mAppComponent;
    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        AppInjector.init(this);
    }

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }

    public static AppComponent getComponent() {
        return mAppComponent;
    }
}
