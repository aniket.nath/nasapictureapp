package com.example.nasapictureapp.dependencyinjection;

/**
 * Marks an activity / fragment injectable.
 */
public interface Injectable {
}
