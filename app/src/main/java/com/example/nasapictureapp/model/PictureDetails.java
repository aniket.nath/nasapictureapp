
package com.example.nasapictureapp.model;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

public class PictureDetails implements Parcelable {

    @SerializedName("copyright")
    private String mCopyright;
    @SerializedName("date")
    private String mDate;
    @SerializedName("explanation")
    private String mExplanation;
    @SerializedName("hdurl")
    private String mHdurl;
    @SerializedName("media_type")
    private String mMediaType;
    @SerializedName("service_version")
    private String mServiceVersion;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("url")
    private String mUrl;

    private PictureDetails(Parcel in) {
        mCopyright = in.readString();
        mDate = in.readString();
        mExplanation = in.readString();
        mHdurl = in.readString();
        mMediaType = in.readString();
        mServiceVersion = in.readString();
        mTitle = in.readString();
        mUrl = in.readString();
    }

    public static final Creator<PictureDetails> CREATOR = new Creator<PictureDetails>() {
        @Override
        public PictureDetails createFromParcel(Parcel in) {
            return new PictureDetails(in);
        }

        @Override
        public PictureDetails[] newArray(int size) {
            return new PictureDetails[size];
        }
    };

    public String getCopyright() {
        return mCopyright;
    }

    public void setCopyright(String copyright) {
        mCopyright = copyright;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getExplanation() {
        return mExplanation;
    }

    public void setExplanation(String explanation) {
        mExplanation = explanation;
    }

    public String getHdurl() {
        return mHdurl;
    }

    public void setHdurl(String hdurl) {
        mHdurl = hdurl;
    }

    public String getMediaType() {
        return mMediaType;
    }

    public void setMediaType(String mediaType) {
        mMediaType = mediaType;
    }

    public String getServiceVersion() {
        return mServiceVersion;
    }

    public void setServiceVersion(String serviceVersion) {
        mServiceVersion = serviceVersion;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mCopyright);
        dest.writeString(mDate);
        dest.writeString(mExplanation);
        dest.writeString(mHdurl);
        dest.writeString(mMediaType);
        dest.writeString(mServiceVersion);
        dest.writeString(mTitle);
        dest.writeString(mUrl);
    }
}
