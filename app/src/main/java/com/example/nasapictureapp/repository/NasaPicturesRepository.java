package com.example.nasapictureapp.repository;

import com.example.nasapictureapp.model.PictureDetails;
import com.example.nasapictureapp.utils.ReadFileDataUtils;
import android.annotation.SuppressLint;
import java.util.List;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class NasaPicturesRepository {

    @SuppressLint("StaticFieldLeak")
    private static NasaPicturesRepository mNasaPicturesRepository = null;

    private NasaPicturesRepository() {
    }

    public static NasaPicturesRepository getInstance() {
        if (mNasaPicturesRepository == null) {
            mNasaPicturesRepository = new NasaPicturesRepository();
        }
        return mNasaPicturesRepository;
    }

    public LiveData<List<PictureDetails>> getPictureDetailsList() {
        final MutableLiveData<List<PictureDetails>> data = new MutableLiveData<>();
        List<PictureDetails> pictureDetails =
                ReadFileDataUtils.getInstance().getPictureDetailsList();
        data.setValue(pictureDetails);
        return data;
    }
}
