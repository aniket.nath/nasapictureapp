package com.example.nasapictureapp.ui;

import com.example.nasapictureapp.R;
import com.example.nasapictureapp.adapter.NasaPicturesAdapter;
import com.example.nasapictureapp.model.PictureDetails;
import com.example.nasapictureapp.viewmodel.NasaPicturesViewModel;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.List;

public class NasaPicturesListActivity extends AppCompatActivity  {

    private NasaPicturesAdapter mNasaPicturesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nasa_picture_list);
        setUpRecyclerView();
        setUpViewModel();
    }

    private void setUpRecyclerView() {
        RecyclerView nasaPicturesList = findViewById(R.id.nasa_pictures_list);
        mNasaPicturesAdapter = new NasaPicturesAdapter(this);
        nasaPicturesList.setLayoutManager(new GridLayoutManager(this, 2));
        nasaPicturesList.setAdapter(mNasaPicturesAdapter);
    }

    private void setUpViewModel() {
        NasaPicturesViewModel viewModel = ViewModelProviders.of(this)
                .get(NasaPicturesViewModel.class);
        viewModel.getPictureDetailsList().observe(this, new Observer<List<PictureDetails>>() {
            @Override
            public void onChanged(@Nullable List<PictureDetails> responses) {
                if (responses != null && responses.size() > 0) {
                    mNasaPicturesAdapter.setNasaPicturesList(responses);
                }
            }
        });

    }
}
