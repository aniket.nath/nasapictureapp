package com.example.nasapictureapp.viewmodel;

import com.example.nasapictureapp.model.PictureDetails;
import com.example.nasapictureapp.repository.NasaPicturesRepository;
import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class NasaPicturesViewModel extends AndroidViewModel {

    private LiveData<List<PictureDetails>> mPictureDetailsListLiveData = null;

    public NasaPicturesViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<List<PictureDetails>> getPictureDetailsList() {
        if (mPictureDetailsListLiveData == null) {
            mPictureDetailsListLiveData = NasaPicturesRepository.getInstance()
                    .getPictureDetailsList();
        }
        return mPictureDetailsListLiveData;
    }
}
